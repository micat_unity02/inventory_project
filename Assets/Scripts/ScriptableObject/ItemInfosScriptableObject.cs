using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 아이템 정보를 정의하기 위한 ScriptableObject 클래스입니다.
/// </summary>
[CreateAssetMenu(fileName = "ItemInfos", menuName = "Scriptable Object", order = int.MaxValue)]
public class ItemInfosScriptableObject : ScriptableObject
{
	[Header("# 아이템 정보")]
	public List<ItemInfoElement> m_ItemInfoElement;

	public ItemInfoElement GetItem(string itemCode)
	{
		return m_ItemInfoElement.Find((itemElem) => itemElem.m_ItemCode == itemCode);
	}
}

/// <summary>
/// 아이템 정보와 코드를 한 쌍으로 사용하기 위한 클래스입니다.
/// </summary>
[System.Serializable]
public class ItemInfoElement
{
	[Header("# 아이템 코드")]
	public string m_ItemCode;

	[Header("# 아이템 정보")]
	public ItemInfo m_ItemInfo;
}

/// <summary>
/// 아이템 정보를 나타내는 클래스입니다.
/// </summary>
[System.Serializable]
public class ItemInfo
{
	[Header("# 아이템 이미지")]
	public Sprite m_ItemSprite;

	[Header("# 아이템 이름")]
	public string m_ItemName;
}


